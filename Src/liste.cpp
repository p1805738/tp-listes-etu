#include "liste.hpp"

#include <iostream>
#include <cassert>

Liste::Liste() :
ad(nullptr), _taille(0)
{
  /* votre code ici */
}

Liste::Liste(const Liste& autre) :
ad(nullptr), _taille(0)
{
  const Cellule * curr = autre.tete();
  
  while(curr != nullptr) {
    this->ajouter_en_queue(curr->valeur);
    curr = curr->suivante;
  }

}

Liste& Liste::operator=(const Liste& autre) 
{
  _taille = 0;
  if(ad != nullptr) {
    delete ad;
    ad = nullptr;
  }

  const Cellule * curr = autre.tete();
  
  while(curr != nullptr) {
    this->ajouter_en_queue(curr->valeur);
    curr = curr->suivante;
  }
  
  return *this ;
}

Liste::~Liste() {
  /* votre code ici */
}

void Liste::ajouter_en_tete(int valeur) 
{

  Cellule * cellule_tete = new Cellule(valeur, tete());
  ad = cellule_tete;
  _taille += 1;

}

void Liste::ajouter_en_queue(int valeur)
{
  Cellule * cellule_queue = new Cellule(valeur);
  Cellule * curr = ad;
  if(curr == nullptr) {
    ajouter_en_tete(valeur);
  }
  else {
    while(curr->suivante != nullptr) {
      curr = curr->suivante;
    }
    curr->suivante = cellule_queue;
  }
  _taille += 1;
}

void Liste::supprimer_en_tete() 
{
  _taille -= 1;
  Cellule * cellule_a_suppr = ad;
  ad = ad->suivante;
  cellule_a_suppr->suivante = nullptr;
  delete cellule_a_suppr;
}

Cellule* Liste::tete() 
{

  return ad ;
}

const Cellule* Liste::tete() const 
{

  return ad ;
}

Cellule* Liste::queue() 
{
  Cellule * curr = ad;
  while(curr->suivante != nullptr) {
    curr = curr->suivante;
  }

  return curr ;
}

const Cellule* Liste::queue() const 
{
  Cellule * curr = ad;
  while(curr->suivante != nullptr) {
    curr = curr->suivante;
  }
  return curr ;
}

int Liste::taille() const {
  /* votre code ici */
  return _taille ;
}

Cellule* Liste::recherche(int valeur) 
{
  Cellule * curr = ad;
  while(curr != nullptr) {
    if(curr->valeur == valeur) {
      return curr;
    }
    else {
      curr = curr->suivante;
    }
  }
  return nullptr ;
}

const Cellule* Liste::recherche(int valeur) const 
{
  Cellule * curr = ad;
  while(curr != nullptr) {
    if(curr->valeur == valeur) {
      return curr;
    }
    else {
      curr = curr->suivante;
    }
  }
  return nullptr ;
}

void Liste::afficher() const 
{
  std::cout << "Affichage : ";
  if(ad != nullptr) {

    std::cout << "[ ";

    Cellule* curr = ad;

    while(curr != nullptr) {
      std::cout << curr->valeur << " ";
      curr = curr->suivante;
    }

    std::cout << "]" << std::endl;
  }
}
