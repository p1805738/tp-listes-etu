#ifndef LIFAP6_LISTE_CELLULE_HPP
#define LIFAP6_LISTE_CELLULE_HPP

class Cellule {

  /* votre code ici */
  public : 
    int valeur;
    Cellule * suivante;

    Cellule(int _valeur=0, Cellule * _suivante = nullptr);
    ~Cellule();
} ;

#endif
