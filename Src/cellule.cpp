#include "cellule.hpp"

Cellule::Cellule(int _valeur, Cellule * _suivante) :
valeur(_valeur),
suivante(_suivante) 
{

}

Cellule::~Cellule() 
{
    if(this->suivante != nullptr) {
        delete this->suivante;
    }
}